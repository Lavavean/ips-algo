﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing countOutOfLineThrough(h,k) function", function(){
    
	var ps = 
	[
		//parallel lines
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 1440695n, y: 4398240n },
			{ x:-2356805n, y:-3581760n },
			{ x: -293505n, y: 5943840n },
			{ x: 2850675n, y: 3141600n },
			{ x: 1116475n, y: 4687200n },
		]),
		//intersecting lines
		new PointSet(
		1n,
		1n,
		[
			{ x:-2n, y:-2n },
			{ x: 0n, y: 2n },
			{ x: 3n, y: 5n },
			{ x: 1n, y: 4n },
			{ x: 4n, y: 2n },
			{ x: 5n, y:-1n },
			{ x:-1n, y: 0n },
			{ x: 6n, y:-4n },
		]),
		//intersecting with common point
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 2n, y:-1n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 2614385n, y: 4411680n },
			{ x:-2335625n, y:       0n },
			{ x:-1956875n, y:       0n },
			{ x: -694375n, y:       0n },
			{ x:-1285225n, y: -787800n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x:  730825n, y: 6333600n },
			{ x:-2647095n, y: 3323040n },
			{ x: 2155885n, y: 7603680n },
			{ x:-4019375n, y: 2100000n },
			{ x:-5875625n, y: 2475000n },
		]),
		//parallel with extra point
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 1n, y:-1n },
			{ x: 3n, y:-1n },
		]),
		//intersecting with common point and extra point
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 2n, y:-1n },
			{ x: 4n, y: 3n },
		]),
		//intersecting with extra point not in the end of PS
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 4n, y: 3n },
			{ x: 2n, y:-1n },
		]),
		new PointSet(//extra point
		21n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 5199375n, y: 1100000n },
			{ x:   85625n, y: 1275000n },
			{ x: 2305625n, y:  825000n },
			{ x: 1024375n, y:-1500000n },
			{ x: 4155625n, y:  450000n },
		])
	];

    it("Should return rigth count of points lying out of line through given points", function(done){
        expect(ps[0].countOutOfLineThrough(5, 6)).to.eql(4);
        expect(ps[1].countOutOfLineThrough(0, 1)).to.eql(5);
        expect(ps[2].countOutOfLineThrough(0, 1)).to.eql(4);
        expect(ps[3].countOutOfLineThrough(0, 6)).to.eql(3);
        expect(ps[4].countOutOfLineThrough(0, 1)).to.eql(2);
        expect(ps[5].countOutOfLineThrough(0, 1)).to.eql(5);
        expect(ps[6].countOutOfLineThrough(5, 7)).to.eql(5);
        expect(ps[7].countOutOfLineThrough(0, 7)).to.eql(6);
        expect(ps[8].countOutOfLineThrough(0, 1)).to.eql(6);
        expect(ps[9].countOutOfLineThrough(0, 1)).to.eql(5);
        done();
    });
    
});
