var mocha  = require('mocha'); 
const assert = require('chai').assert;
const expect = require('chai').expect;
var PointSet = require('../PointSet.js');
var maximizeFaster = require('../maximizer/maximizeFaster.js').maximizeFaster;
var maximizeFasterWithReduce = require('../maximizer/maximizeFasterWithReduce.js').maximizeFasterWithReduce;
var Point = require('../maximizer/Point.js');

describe("Testing maximizeFasterWithReduce() function", function(){
  let triangleKurz= new PointSet(
		1n,
		40n,
		[
      new Point(0n, 0n),
      new Point(800n, 0n),
      new Point(800n, 600n),
		]
    );
    
    let equilateralTriangle = new PointSet(
      3n,
      2n,
      [
        new Point(0n, 0n),
        new Point(2n, 0n),
        new Point(1n, 1n),
      ]
    ); 

    let triangleEgypt = new PointSet(
		1n,
		80n,
		[
      new Point(0n, 0n),
      new Point(3200n, 0n),
      new Point(0n, 2400n),
		]
    );

    let trapez = new PointSet(
      15n,
      8n,
      [
        new Point(0n, 0n),
        new Point(32n, 0n),
        new Point(28n, 4n),
        new Point(4n, 4n)
      ]
    );

    let extraTriangle = new PointSet(
      15n,
      4n,
      [
        new Point(0n, 0n),
        new Point(8n, 0n),
        new Point(7n, 1n)
      ]
    );
    
    function isSubSet(arr1, arr2){
      for(let p of arr2)
          if(!(arr1.find((el) => el.value.x === p.value.x && el.value.y === p.value.y)))
              return false;
      return true;
    }

    function compare(maxArray1, maxArray2){
      return maxArray1.length === maxArray2.length && isSubSet(maxArray1, maxArray2);
    }
    
    describe("objects received after maximizeFaster() and maximizeFasterWithReduce() should be equal", function(){
    it("triangleEgypt", function(done){
        expect(compare(maximizeFaster(triangleEgypt), maximizeFasterWithReduce(triangleEgypt))).to.eql(true);
        done();
    });
    it("triangleKurz", function(done){
        expect(compare(maximizeFaster(triangleKurz), maximizeFasterWithReduce(triangleKurz))).to.eql(true);
        done();
    });
    it("trapez", function(done){
        expect(compare(maximizeFaster(trapez), maximizeFasterWithReduce(trapez))).to.eql(true);
        done();
    });
    it("extraTriangle", function(done){
        expect(compare(maximizeFaster(extraTriangle), maximizeFasterWithReduce(extraTriangle))).to.eql(true);
        done();
    });
    it("equilateralTriangle", function(done){
        expect(compare(maximizeFaster(equilateralTriangle), maximizeFasterWithReduce(equilateralTriangle))).to.eql(true);
        done();
    });
  });
});