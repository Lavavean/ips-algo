﻿var mocha  = require('mocha'); 
const assert = require('chai').assert;
const expect = require('chai').expect;
var PointSet = require('../PointSet.js');

describe( "Testing isFourOnCircle() function", function(){
	const ps = {
		heptagon_1:{
		  char: 2002n,
		  base: 11560n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 66816800n,
			  y: 0n,
			},
			{
			  x: -47026080n,
			  y: 1109760n,
			},
			{
			  x: 39581440n,
			  y: 2219520n,
			},
			{
			  x: -165839760n,
			  y: -1248480n,
			},
			{
			  x: 61846000n,
			  y: -3468000n,
			},
			{
			  x: -35893800n,
			  y: -1734000n,
			},
		  ],
	  },
	
		heptagon_2:{
		  char: 2002n,
		  base: 33274n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 553579538n,
			  y: 0n,
			},
			{
			  x: -419289484n,
			  y: -11612880n,
			},
			{
			  x: -1659973072n,
			  y: 1486080n,
			},
			{
			  x: 268580108n,
			  y: -23225760n,
			},
			{
			  x: 285764972n,
			  y: 24711840n,
			},
			{
			  x: -410697052n,
			  y: 12355920n,
			},
		  ],
	  },
	
		hexagon_1:{
		  char: 2002n,
		  base: 136n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 9248n,
			  y: 0n,
			},
			{
			  x: -4280n,
			  y: 240n,
			},
			{
			  x: 9936n,
			  y: 480n,
			},
			{
			  x: 688n,
			  y: 480n,
			},
			{
			  x: 14216n,
			  y: 240n,
			},
		  ],
	  },
	
	  hexagon_2:{
		  char: 2002n,
		  base: 254n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 32258n,
			  y: 0n,
			},
			{
			  x: -8326n,
			  y: 720n,
			},
			{
			  x: 47864n,
			  y: 1440n,
			},
			{
			  x: 15606n,
			  y: 1440n,
			},
			{
			  x: 56190n,
			  y: 720n,
			},
		  ],
	  },
		
	  pentagon_1:{
			char: 55n,
			base: 52n,
			p: [
			  {
				x: 0n,
				y: 0n,
			  },
			  {
				x: 1352n,
				y: 0n,
			  },
			  {
				x: 380n,
				y: -240n,
			  },
			  {
				x: 2036n,
				y: -432n,
			  },
			  {
				x: 3008n,
				y: -192n,
			  },
			],
	  },
	
	  pentagon_2:{
		  char: 1n,
		  base: 64n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 2048n,
			  y: 0n,
			},
			{
			  x: 1024n,
			  y: -1920n,
			},
			{
			  x: 4608n,
			  y: -1920n,
			},
			{
			  x: 1024n,
			  y: -4032n,
			},
		  ],
	  },
	
	  trapez:{
		  char: 15n,
			  base: 8n,
			  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 32n,
			  y: 0n,
			},
			{
			  x: 28n,
			  y: 4n,
			},
			{
			  x: 4n,
			  y: 4n,
			}
			  ]
	  },
	
	  rectangle:{
		char: 1n,
		base: 16n,
		p: [
		  {
			x: 0n,
			y: 0n,
		  },
		  {
			x: 128n,
			y: 0n,
		  },
		  {
			x: 128n,
			y: 96n,
		  },
		  {
			x: 0n,
			y: 96n,
		  },
		  {
			x: 64n,
			y: 48n,
		  }
		]
	  },
	
	  eqTriangle:{
		  char: 3n,
		  base: 2n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 2n,
			  y: 0n,
			},
			{
			  x: 1n,
			  y: 1n,
			}
		  ]
	  }
	};

	function getPS(obj){
		return new PointSet(obj.char, obj.base, obj.p);
	}
	
	it("For both heptagons function must return false", function(done){
        expect(getPS(ps.heptagon_1).isFourOnCircle()).to.eql(false);
		expect(getPS(ps.heptagon_2).isFourOnCircle()).to.eql(false);
        done();
    });
	it("For both hexagons function must return false", function(done){
        expect(getPS(ps.hexagon_1).isFourOnCircle()).to.eql(false);
		expect(getPS(ps.hexagon_1).isFourOnCircle()).to.eql(false);
        done();
    });
	it("For both pentagons function must return false", function(done){
        expect(getPS(ps.pentagon_1).isFourOnCircle()).to.eql(false);
		expect(getPS(ps.pentagon_2).isFourOnCircle()).to.eql(false);
        done();
    });
	it("For trapez and rectangle function must return true", function(done){
        expect(getPS(ps.rectangle).isFourOnCircle()).to.eql(true);
		expect(getPS(ps.trapez).isFourOnCircle()).to.eql(true);
        done();
    });
	it("For sets consisting of less than 4 points function mast return false", function(done){
        expect(getPS(ps.eqTriangle).isFourOnCircle()).to.eql(false);
        done();
    });
});

