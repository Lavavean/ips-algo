var mocha  = require('mocha'); 
const assert = require('chai').assert;
const expect = require('chai').expect;
var PointSet = require('../PointSet.js');
var maximizeFaster = require('../maximizer/maximizeFaster.js').maximizeFaster;
var Point = require('../maximizer/Point.js');
var arrayOut = require('../maximizer/maximizerUtilities.js').arrayOut;

describe("Testing maximizeFaster() function", function(){
    let triangleKurz= new PointSet(
		1n,
		1n,
		[
            new Point(0n, 0n),
            new Point(20n, 0n),
            new Point(20n, 15n),
		]
    );
    let Equilateraltriangle = new PointSet(
        3n,
        2n,
        [
            new Point(0n, 0n),
            new Point(2n, 0n),
            new Point(1n, 1n),
        ]
    ); 
    let triangleEgypt = new PointSet(
		1n,
		1n,
		[
            new Point(0n, 0n),
            new Point(4n, 0n),
            new Point(0n, 3n),
		]
    );

    let trapez = new PointSet(
      15n,
      8n,
      [
        new Point(0n, 0n),
        new Point(32n, 0n),
        new Point(28n, 4n),
        new Point(4n, 4n)
      ]
    );

  const resultEgypt = [
      {
        value: {
          x: 4n,
          y: 3n,
        },
        m: 2n,
        n: 1n,
      },
      {
        value: {
          x: -4n,
          y: 0n,
        },
        m: 4n,
        n: 1n,
      },
  ];
  const resultKurz = [
      {
        value: {
          x: 20n,
          y: -99n,
        },
        m: 2n,
        n: -13n,
      },
      {
        value: {
          x: 20n,
          y: 99n,
        },
        m: 2n,
        n: 17n,
      },
      {
        value: {
          x: 20n,
          y: -48n,
        },
        m: 4n,
        n: -11n,
      },
      {
        value: {
          x: 12n,
          y: 0n,
        },
        m: 4n,
        n: -5n,
      },
      {
        value: {
          x: 20n,
          y: 48n,
        },
        m: 4n,
        n: 19n,
      },
      {
        value: {
          x: 20n,
          y: -21n,
        },
        m: 8n,
        n: -7n,
      },
      {
        value: {
          x: -36n,
          y: 105n,
        },
        m: 8n,
        n: -5n,
      },
      {
        value: {
          x: 20n,
          y: 21n,
        },
        m: 8n,
        n: 23n,
      },
      {
        value: {
          x: 20n,
          y: -15n,
        },
        m: 10n,
        n: -5n,
      },
      {
        value: {
          x: 0n,
          y: 15n,
        },
        m: 10n,
        n: 5n,
      },
      {
        value: {
          x: 12n,
          y: 0n,
        },
        m: 20n,
        n: 5n,
      },
      {
        value: {
          x: 28n,
          y: 0n,
        },
        m: 20n,
        n: 11n,
      },
      {
        value: {
          x: 40n,
          y: 0n,
        },
        m: 20n,
        n: 15n,
      },
      {
        value: {
          x: 56n,
          y: 0n,
        },
        m: 20n,
        n: 17n,
      },
      {
        value: {
          x: 132n,
          y: 0n,
        },
        m: 20n,
        n: 19n,
      },
      {
        value: {
          x: -92n,
          y: 0n,
        },
        m: 20n,
        n: 21n,
      },
      {
        value: {
          x: -16n,
          y: 0n,
        },
        m: 20n,
        n: 23n,
      },
  ];
  const resultTrapez = [
    {
      value: {
        x: 16n,
        y: 16n,
      },
      m: 0n,
      n: 16n,
    },
    {
      value: {
        x: 21n,
        y: 3n,
      },
      m: 8n,
      n: 32n,
    },
    {
      value: {
        x: 49n,
        y: 7n,
      },
      m: 24n,
      n: 32n,
    },
    {
      value: {
        x: 11n,
        y: -3n,
      },
      m: 8n,
      n: 16n,
    },
    {
      value: {
        x: 21n,
        y: 3n,
      },
      m: 8n,
      n: 16n,
    },
    {
      value: {
        x: 44n,
        y: -12n,
      },
      m: 16n,
      n: 0n,
    },
    {
      value: {
        x: 4n,
        y: 4n,
      },
      m: 16n,
      n: 8n,
    },
    {
      value: {
        x: 24n,
        y: 0n,
      },
      m: 16n,
      n: 8n,
    },
    {
      value: {
        x: 24n,
        y: 0n,
      },
      m: 32n,
      n: 8n,
    },
    {
      value: {
        x: 56n,
        y: 0n,
      },
      m: 32n,
      n: 24n,
    },
  ];

  function isSubSet(arr1, arr2){
      for(let p of arr2)
          if(!(arr1.find((el) => el.value.x === p.value.x && el.value.y === p.value.y)))
              return false;
      return true;
  }

    it("resultEgypt must be a subset of maximizeFaster(triangleEgypt) result", function(done){
        expect(isSubSet(resultEgypt, maximizeFaster(triangleEgypt))).to.eql(true);
        done();
    });

    it("resultKurz must be a subset of maximizeFaster(triangleKurz) result", function(done){
      expect(isSubSet(resultKurz, maximizeFaster(triangleKurz))).to.eql(true);
      done();
    });

    it("resultTrapez must be a subset of maximizeFaster(trapez) result", function(done){
      expect(isSubSet(resultTrapez, maximizeFaster(trapez))).to.eql(true);
      done();
    });

    it("For Equilateral triangle number of new points equal 0 ", function(done){
      expect(maximizeFaster(Equilateraltriangle).length).to.eql(0); 
      done();
      });
});
