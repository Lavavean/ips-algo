var mocha  = require('mocha'); 
const assert = require('chai').assert;
const expect = require('chai').expect;
var PointSet = require('../PointSet.js');
var maximizeFaster = require('../maximizer/maximizeFaster.js').maximizeFaster;
var multyThreadsMaximizer = require('../maximizer/multyThreadsMaximizer.js').multyThreadsMaximizer;
var Point = require('../maximizer/Point.js');

describe("Testing multyThreadsMaximizer() function", function(){
    const ps = {
		hexagon_1:{
		  char: 2002n,
		  base: 136n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 9248n,
			  y: 0n,
			},
			{
			  x: -4280n,
			  y: 240n,
			},
			{
			  x: 9936n,
			  y: 480n,
			},
			{
			  x: 688n,
			  y: 480n,
			},
			{
			  x: 14216n,
			  y: 240n,
			},
		  ],
	  },
	
	  hexagon_2:{
		  char: 2002n,
		  base: 254n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 32258n,
			  y: 0n,
			},
			{
			  x: -8326n,
			  y: 720n,
			},
			{
			  x: 47864n,
			  y: 1440n,
			},
			{
			  x: 15606n,
			  y: 1440n,
			},
			{
			  x: 56190n,
			  y: 720n,
			},
		  ],
	  },
		
	  pentagon_1:{
			char: 55n,
			base: 52n,
			p: [
			  {
				x: 0n,
				y: 0n,
			  },
			  {
				x: 1352n,
				y: 0n,
			  },
			  {
				x: 380n,
				y: -240n,
			  },
			  {
				x: 2036n,
				y: -432n,
			  },
			  {
				x: 3008n,
				y: -192n,
			  },
			],
	  },
	
	  pentagon_2:{
		  char: 1n,
		  base: 64n,
		  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 2048n,
			  y: 0n,
			},
			{
			  x: 1024n,
			  y: -1920n,
			},
			{
			  x: 4608n,
			  y: -1920n,
			},
			{
			  x: 1024n,
			  y: -4032n,
			},
		  ],
	  },
	
	  trapez:{
		  char: 15n,
			  base: 8n,
			  p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 32n,
			  y: 0n,
			},
			{
			  x: 28n,
			  y: 4n,
			},
			{
			  x: 4n,
			  y: 4n,
			}
			  ]
	  },
	
	  rectangle:{
		char: 1n,
		base: 16n,
		p: [
		  {
			x: 0n,
			y: 0n,
		  },
		  {
			x: 128n,
			y: 0n,
		  },
		  {
			x: 128n,
			y: 96n,
		  },
		  {
			x: 0n,
			y: 96n,
		  },
		  {
			x: 64n,
			y: 48n,
		  }
		]
	  },
	
	  eqTriangle:{
		char: 3n,
		base: 2n,
		p: [
			{
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 2n,
			  y: 0n,
			},
			{
			  x: 1n,
			  y: 1n,
			}
		]
	  },
      triangleKurz:{
        char: 1n,
		base: 8n,
		p: [
		    {
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 32n,
			  y: 0n,
			},
			{
			  x: 32n,
			  y: 24n,
			}
		]
      },
      triangleEgypt:{
        char: 1n,
		base: 8n,
		p: [
		    {
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 32n,
			  y: 0n,
			},
			{
			  x: 0n,
			  y: 24n,
			}
		]
      },
      extraTriangle:{
        char: 15n,
		base: 4n,
		p: [
		    {
			  x: 0n,
			  y: 0n,
			},
			{
			  x: 8n,
			  y: 0n,
			},
			{
			  x: 7n,
			  y: 1n,
			}
		]
      }  
	};

	function getPS(obj){
		return new PointSet(obj.char, obj.base, obj.p.map(el => new Point(el.x, el.y)));
	}

    function isSubSet(arr1, arr2){
        for(let p of arr2)
            if(!(arr1.find((el) => el.value.x === p.value.x && el.value.y === p.value.y)))
                return false;
        return true;
    }
  
    function compare(maxArray1, maxArray2){
        return maxArray1.length === maxArray2.length && isSubSet(maxArray1, maxArray2);
    }
    
    describe("Objects received after maximizeFaster() and multyThreadsMaximizer() should be equal", function(){
        for(let core=1n; core<=8n; core++){
            describe(`Test for ${core} threads:`, function(){
                for(let p in ps)
                    it(`for ${p}`, function(){
                        return multyThreadsMaximizer(getPS(ps[p]), core).then(
                            function(resolve){
                                expect(compare(resolve, maximizeFaster(getPS(ps[p])))).to.equal(true);
                            }
                        );
                    });
            });
        }
    });
});