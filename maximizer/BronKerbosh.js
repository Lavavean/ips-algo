const PointSet = require('../PointSet.js');

PointSet.prototype.newGeneration = function(arr, v){
    return arr.filter(a => this.isDistanceIntegral(a, v));
}

PointSet.prototype.notCond = function(candidates, not){  //not НЕ содержит вершины, СОЕДИНЕННОЙ СО ВСЕМИ вершинами из candidates
    let flag = true;

    for(let p of not){
        for(let c of candidates)
            flag = flag && this.isDistanceIntegral(p, c);
        if(flag)
            return false;
    }

    return true;
}

function getCandidates(ps, newPoints){
    let candidates = [];
    for(let p of ps.p)
        candidates.push(p);
    for(let p of newPoints)
        candidates.push(p.value);
    return candidates;
}

function findMaxCliqueLength(cliques){
    let max = cliques[0].p.length;
    for(let c of cliques){
        if(c.p.length > max)
            max = c.p.length;
    }
    return max;
}

function getResultFromCliques(cliques){
    let result = [];
    let max = findMaxCliqueLength(cliques);
    for(let c of cliques){
        if(c.p.length === max)
            result.push(c);
    }
    return result;
}


function BronKerbosh(ps, newPoints, compsub, candidates){
    compsub = compsub || [];
    candidates =  candidates || getCandidates(ps, newPoints);
    let cliques = [];

    PointSet.prototype.findCliques = function(candidates, not){
        while(candidates.length && this.notCond(candidates, not)){
            let v = candidates[0];
            compsub.push(v);
            let newCandidates = this.newGeneration(candidates, v);
            let newNot = this.newGeneration(not, v);
            
            if(!newCandidates.length && !newNot.length)
                cliques.push(new PointSet(this.char, this.base, compsub.slice()));
            else
                this.findCliques(newCandidates, newNot);
            
            compsub = compsub.filter(p => !(p.x == v.x && p.y == v.y));
            candidates = candidates.filter(p => !(p.x == v.x && p.y == v.y));
            not.push(v);
        }
    }

    ps.findCliques(candidates, []);

    return getResultFromCliques(cliques);
}

module.exports = {BronKerbosh}