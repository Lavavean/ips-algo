const { resolve } = require('path');
const PointSet = require('../PointSet.js');
const Point = require('./Point.js');
const maximizeFasterWithReduce = require('./maximizeFasterWithReduce.js').maximizeFasterWithReduce    ;
const createDistanceMatrixFromStr = require('./matrixToCoord.js').createDistanceMatrixFromStr;
const createPSFromMatrix = require('./matrixToCoord.js').createPSFromMatrix;
const BronKerboshAdvanced = require('./BronKerboshAdvanced.js').BronKerboshAdvanced;
const BronKerbosh = require('./BronKerbosh.js').BronKerbosh;
const PSWrite = require('./PSWrite.js');
const multyThreadsMaximizer = require('./multyThreadsMaximizer.js').multyThreadsMaximizer;
let fs = require('fs');

function getMatrixLength(matrix){
	let s = matrix.length;
	return (1 + Math.sqrt(1+8*s))/2
}

function getPSFromMatrix(matrix){
	if(typeof(matrix) === 'string'){
		matrix = matrix.split(' ');
		for(let i in matrix)
			matrix[i] = BigInt(matrix[i]);
		return createPSFromMatrix(createDistanceMatrixFromStr(matrix, getMatrixLength(matrix)));
	}
	else
		return createPSFromMatrix(matrix);
}

function getPSToMaximize(matrix, path, dirName){
	let result = getPSFromMatrix(matrix).chooseOptimalPointsForMaximize(true).putPointsToXAxis(0, 1);
	try{
		fs.accessSync(path+dirName, fs.constants.F_OK);
		return result;
	}
	catch(err){
		PSWrite(result, path+dirName + '/oldPointSet');
		return result;
	}	
}

function getNewPoints(PSToMaximize, path, dirName){
	try{
		fs.accessSync(path+dirName+'/newPoints.json', fs.constants.F_OK);
		return readNewPoints(path+dirName+'/newPoints.json');
	}
	catch(err){
		let newPoints = maximizeFasterWithReduce(PSToMaximize);
		objWrite(newPoints, path+dirName+'/newPoints');
		return newPoints;
	}
}

function getPSExtraPoints(PSToMaximize, newPoints, path, dirName){
	try{
		fs.accessSync(path+dirName+'/PSExtraPoints.json', fs.constants.F_OK);
		return readExtraPoints(path+dirName+'/PSExtraPoints.json');
	}
	catch(err){
		let PSExtraPoints = PSToMaximize.getExtraPoints(newPoints);
		objWrite(PSExtraPoints, path+dirName+'/PSExtraPoints');
		return PSExtraPoints;
	}
}

function getDirDate(){
	let today = new Date();
	let res = [today.getDate(), today.getMonth()+1, today.getFullYear(), today.getHours(), today.getMinutes()];
	for(let i in res)
		res[i] = res[i] < 10 ? '0' + res[i] : res[i];
	return	res[0] + '.' + res[1] + '.' + res[2] + '(' + res[3] + '-' + res[4] + ')';
	
}

function objWrite(newPoints, path){
	let data = JSON.stringify(newPoints, (key, value) =>
	typeof value === 'bigint'
		? value.toString()
		: value // return everything else unchanged
	);
	fs.writeFileSync(path+'.json', data);
}


function getArraysForBK(PSToMaximize, canChangeOnePoint){
	let candidates = canChangeOnePoint.slice();
	let compsub = [];
	for(let p of PSToMaximize.p){
		if(canChangeOnePoint.find(el => !PSToMaximize.isDistanceIntegral(el, p)) && !candidates.includes(p))
			candidates.push(p);
		else
			compsub.push(p);
	}
	return {candidates: candidates, compsub:compsub};
}

function tryToExpand(PSToMaximize, PSExtraPoints, path, dirName){
	if(PSExtraPoints.newPoints.length){
		let extendedPS = BronKerbosh(PSToMaximize, undefined, PSToMaximize.p.slice(), PSExtraPoints.newPoints.slice());
		for(let i=0; i<extendedPS.length; i++){
			try{
				fs.accessSync(path+dirName+'/extendedPS#'+(i+1), fs.constants.F_OK);
			}
			catch(err){
				PSWrite(extendedPS[i], path+dirName+'/extendedPS#'+(i+1));
			}
		}
	}
}

function tryChangeOnePoint(PSToMaximize, PSExtraPoints, path, dirName){
	if(PSExtraPoints.canChangeOnePoint.length){
		let arraysForBK = getArraysForBK(PSToMaximize, PSExtraPoints.canChangeOnePoint);
		let onePointChangedPS = BronKerbosh(PSToMaximize, undefined, arraysForBK.compsub, arraysForBK.candidates);
		for(let i=0; i<onePointChangedPS.length; i++){
			try{
				fs.accessSync(path+dirName+'/onePointChangedPS#'+(i+1), fs.constants.F_OK);
			}
			catch(err){
				PSWrite(onePointChangedPS[i], path+dirName+'/onePointChangedPS#'+(i+1));
			}
		}
	}
}

function readNewPoints(path){
	let rawdata = fs.readFileSync(path);
	let parseddata= JSON.parse(rawdata);
	for(let d of parseddata){
		d.value.x = BigInt(d.value.x);
		d.value.y = BigInt(d.value.y);
		d.m = BigInt(d.m);
		d.n = BigInt(d.n);
	}
	return parseddata;
}

function readExtraPoints(path){
	let rawdata = fs.readFileSync(path);
	let parseddata= JSON.parse(rawdata);
	for(let i in parseddata){
		for(let p of parseddata[i]){
			p.x = BigInt(p.x);
			p.y = BigInt(p.y);
		}
	}
	return parseddata;
} 

function stretch(matrix, n){
	if(typeof(matrix) === 'string'){
		let temp = matrix.split(' ');
		for(let i in temp)
			temp[i] = n*BigInt(temp[i]);
		return temp.join(' ');
	}
	else{
		for(let i=0; i<matrix.length; i++)
			for(let j=0; j<matrix.length; j++)
				matrix[i][j] *= n;
		return matrix;
	}
}

function getNewPointsAsync(PSToMaximize, path, dirName, cores){
	try{
		fs.accessSync(path+dirName+'/newPoints.json', fs.constants.F_OK);
		return readNewPoints(path+dirName+'/newPoints.json');
	}
	catch(err){
		let newPoints = multyThreadsMaximizer(PSToMaximize, cores);
		newPoints.then(resolve => objWrite(resolve, path+dirName+'/newPoints'));
		return newPoints;
	}
}

function molotilka(matrix, path = './maximizer/myTest/SavedСalculations/', dirName = getDirDate(), opt = {stretch: 1n, cores: 1n}){
	matrix = stretch(matrix, opt.stretch);

	let PSToMaximize = getPSToMaximize(matrix, path, dirName);
	if(opt.cores === 1n){
		let newPoints = getNewPoints(PSToMaximize, path, dirName);
		let PSExtraPoints = getPSExtraPoints(PSToMaximize, newPoints, path, dirName);
		tryToExpand(PSToMaximize, PSExtraPoints, path, dirName);
		tryChangeOnePoint(PSToMaximize, PSExtraPoints, path, dirName)
	}
	else if(opt.cores > 1n){
		let newPoints = getNewPointsAsync(PSToMaximize, path, dirName, opt.cores);
		if (typeof(newPoints) === 'object' && typeof(newPoints.then) === 'function'){
			newPoints.then(resolve =>{
				let PSExtraPoints = getPSExtraPoints(PSToMaximize, resolve, path, dirName);
				tryToExpand(PSToMaximize, PSExtraPoints, path, dirName);
				tryChangeOnePoint(PSToMaximize, PSExtraPoints, path, dirName)
				process.exit();
			});
		}
		else{
			let PSExtraPoints = getPSExtraPoints(PSToMaximize, newPoints, path, dirName);
			tryToExpand(PSToMaximize, PSExtraPoints, path, dirName);
			tryChangeOnePoint(PSToMaximize, PSExtraPoints, path, dirName)
		}
	}
}

module.exports = {molotilka}