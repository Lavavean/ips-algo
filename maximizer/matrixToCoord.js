const squarefree =  require('../squarefree');
var Point = require('./Point.js');
var PointSet = require ( '../PointSet.js' );
var sqrt = require('bigint-isqrt');

function heronSq(a, b, c){
    let P = a+b+c;
    return P*(P-2n*a)*(P-2n*b)*(P-2n*c);
}

function createPSFromMatrix(matrix){
    let a = matrix[0][1];
    let b = matrix[0][2];
    let c = matrix[1][2]; 

    let SNum = squarefree(heronSq(a, b, c));
    let q = SNum[1];

    let result = new PointSet(
        q,
        2n*a,
        [new Point(0n, 0n), new Point(a*2n*a, 0n)]
    );

    let y = SNum[0];
    let x = sqrt(4n*a*a*b*b - q*y*y);
    let pt = new Point(x, y);

    if(result.distance(result.p[1], pt) != matrix[2][1])
        pt.y = -pt.y;
    if(result.distance(result.p[1], pt) != matrix[2][1]){
        pt.x = -pt.x;
        pt.y = -pt.y;
    }
    if(result.distance(result.p[1], pt) != matrix[2][1])
        pt.y = -pt.y;

    result.p.push(pt);

    let N = matrix[0].length;
    for(let i = 3; i<N; i++){
        b = matrix[0][i];
        c = matrix[1][i];
        SNum = squarefree(heronSq(a, b, c));
        y = SNum[0];
        q = q || SNum[1];
        x = sqrt(4n*a*a*b*b - q*y*y);

        let pt = new Point(x, y);

        if(result.distance(result.p[i-1], pt) != matrix[i][i-1])
            pt.y = -pt.y;
        if(result.distance(result.p[i-1], pt) != matrix[i][i-1]){
            pt.x = -pt.x;
            pt.y = -pt.y;
        }
        if(result.distance(result.p[i-1], pt) != matrix[i][i-1])
            pt.y = -pt.y;

        result.p.push(pt);
    }
    result.char = q;
    return result;
}

function psOutForCAS(ps){
    let result = ''
    for(let p of ps.p)
        result += '(' + p.x + '/' + ps.base + ', ' + p.y + '*sqrt(' + ps.char + ')/' + ps.base + ')\n';
    return result;
}

function createDistanceMatrixFromStr(a, n){
    let result = [];
    let counter = 0;
    for(let i=0n; i<n; i++){
        let row = [];
        for(let j=0n; j<n; j++){
            if(i==j)
                row[j] = 0n;
            else if(j<i+1n)
                row[j] = result[j][i]
            else{
                row[j] = a[counter];
                counter++;
            }
        }
        result.push(row);
    }
    return result;
}

module.exports = {
    createPSFromMatrix,
    createDistanceMatrixFromStr,
    psOutForCAS,
};

