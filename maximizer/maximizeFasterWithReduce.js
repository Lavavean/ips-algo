const isPerfectSquare = require('bigint-is-perfect-square');
var sqrt = require('bigint-isqrt');
var Point = require('./Point.js');
var RatioNumber = require ( './RatioNumber.js' );
const PointSet = require('../PointSet.js');

function maximizeFasterWithReduce(PS){ // Версия только для точек попадающих на сетку
    let ps = PS.deepClone();
    ps.reduce();
    ps.p[0] = new Point(ps.p[0].x, ps.p[0].y);
    ps.p[1] = new Point(ps.p[1].x, ps.p[1].y);
    ps.p[2] = new Point(ps.p[2].x, ps.p[2].y);
    
    let red = 2n*ps.distance(ps.p[0], ps.p[1])/ps.base;

    const q = ps.char;
    const N = sqrt(ps.qasidistance(ps.p[0], ps.p[2]));
    const M = sqrt(ps.qasidistance(ps.p[0], ps.p[1]));
    const s = ps.p[2].x; 
    const t = ps.p[2].y;
    const step = ps.base; // Требуется доделать!    

    let result = [];
    let x1, x2, y1, y2, D, sqD, temp, num, denum;
    let k, p, l, f, u, r, n2, m2, m3, ks2;

    const s2 = s*s;
    const s3 = s2*s; 
    const M2 = M*M;
    const Ms = M*s;
    const Ms2 = Ms*s;
    const qt = q*t;
    const qt2 = qt*t;
    
    //m=0
    if(M%2n === 0n){ // В противном случае x=d/2 не попадает на сетку!
        x1 = new RatioNumber(M, 2n);

        //n=0
        y1 =  new RatioNumber(s2+qt2-Ms, 2n*qt);
        temp = reviewFaster(x1, y1, q, ps.base, 0n, 0n, ps.p[0], ps.p[1], ps.p[2], red);
        if(temp)
            result.push(temp);
 
        //n>0, n<N; Случай когда n<0 можно убрать в силу симметрии
        for(let n=step; n<N; n+=step){ 
            n2 = n*n;
            p = M*s+n2;
            l = s2-p;
            D = q*((l-p+M2)*s2+n2*(Ms+p-M2)+qt2*(2n*l+qt2+M2));
            if(D === 0n){
                y1 =  new RatioNumber((l+qt2)*t, 2n*(qt2-n2));
                temp = reviewFaster(x1, y1, q, ps.base, 0n, n, ps.p[0], ps.p[1], ps.p[2], red);
                if(temp)
                    result.push(temp);
            }
            else if(isPerfectSquare(D)){
                sqD = n*sqrt(D);
                num = (l+qt2)*qt;
                denum = 2n*(qt2-n2)*q;
                y1 = new RatioNumber(num + sqD, denum);
                y2 = new RatioNumber(num - sqD, denum);

                temp = reviewFaster(x1, y1, q, ps.base, 0n, n, ps.p[0], ps.p[1], ps.p[2], red);
                if(temp)
                    result.push(temp);
         
                temp = reviewFaster(x1, y2, q, ps.base, 0n, n, ps.p[0], ps.p[1], ps.p[2], red);
                if(temp)
                    result.push(temp);
            }
        }
        
        //n=N
        y1 =  new RatioNumber(M*t, 2n*s);
        temp = reviewFaster(x1, y1, q, ps.base, 0n, 0n, ps.p[0], ps.p[1], ps.p[2], red);
        if(temp)
             result.push(temp);       
    }

    //0<m<M

        //|n| = N 
    D = s2 + qt2; // Не может быть меньше 0
    if(isPerfectSquare(D)){
        for(let m=step; m<M; m+=step){
            m2 = m*m;
            k = m2 - M2;
            num = k*Ms2;
            denum = 2n*(k*s2+qt2*m2);
            sqD = k*m*s*sqrt(D);

            x1 = new RatioNumber(num + sqD, denum);
            x2 = new RatioNumber(num - sqD, denum);

            y1 = x1.cMultiply(t).cDivide(s);
            y2 = x2.cMultiply(t).cDivide(s);

            temp = reviewFaster(x1, y1, q, ps.base, m, N, ps.p[0], ps.p[1], ps.p[2], red);
            if(temp)
                result.push(temp);  
        
            temp = reviewFaster(x1, y2, q, ps.base, m, N, ps.p[0], ps.p[1], ps.p[2], red);
            if(temp)
                result.push(temp);
        }
    }
        //-N<n<N, При n=0 работают те же самые формулы, см. статью!
    for(let m=step; m<M; m+=step){
        m2 = m*m;
        m3 = m2*m;
        k = M2 - m2; // не зависит от n
        ks2 = k*s2;
        for(let n=-N+step; n<N; n+=step){
            n2 = n*n;
            u = k*(Ms+n2)
            r = M2*(k-m2) + m*(2n*k*n+m3);
            D = q*(n2*(u+Ms*k-r)+s2*(-2n*u+ks2+r)+qt2*(2n*(ks2-u)+k*qt2+r));

            if(D < 0n)
                continue;
            
            if(D === 0n){
                f = m*n+k;
                x1 = new RatioNumber(-(qt2*(M*f-m2*s) - m2*s3 + n*(Ms2*m+f*(m*s-M*n))), 2n*(-k*qt2 + m2*s2 + M*n*(M*n-2n*m*s)));  
                y1 = x1.cMultiply(2n*(m*s-M*n)).cMinus(m*qt2).cMinus(m*s2).cPlus(m*n2).cPlus(k*n).cMultiply(-1n).cDivide(2n*m*qt);  // можно избавиться от умножения на -1n заранее
                temp = reviewFaster(x1, y1, q, ps.base, m, n, ps.p[0], ps.p[1], ps.p[2], red);
                if(temp)
                    result.push(temp);
            }
            else if(isPerfectSquare(D)){
                f = m*n+k;
                num = -(qt2*(M*f-m2*s) - m2*s3 + n*(Ms2*m+f*(m*s-M*n)));
                denum = 2n*(-k*qt2 + m2*s2 + M*n*(M*n-2n*m*s))
                sqD = m*t*sqrt(D);
                x1 = new RatioNumber(num + sqD, denum);
                x2 = new RatioNumber(num - sqD, denum);

                y1 = x1.cMultiply(2n*(m*s-M*n)).cMinus(m*qt2).cMinus(m*s2).cPlus(m*n2).cPlus(k*n).cMultiply(-1n).cDivide(2n*m*qt);
                y2 = x2.cMultiply(2n*(m*s-M*n)).cMinus(m*qt2).cMinus(m*s2).cPlus(m*n2).cPlus(k*n).cMultiply(-1n).cDivide(2n*m*qt);
              
                temp = reviewFaster(x1, y1, q, ps.base, m, n, ps.p[0], ps.p[1], ps.p[2], red);
                if(temp != undefined)
                    result.push(temp); 
                
                temp = reviewFaster(x2, y2, q, ps.base, m, n, ps.p[0], ps.p[1], ps.p[2], red);
                if(temp != undefined)
                    result.push(temp);            
            }
        }
    }

    //m=M
    y1 = new RatioNumber(0n, 1n);
    
    //Если |n| = N, то A и D совпадают.

    f = qt2 + s2;
    // 0<n<N. n<0 можно не рассматривать так как x1(n)=x2(-n)
    for(let n=step; n<N; n+=step){
        n2 = n*n;
        num = n2 - f;
        x1 = new RatioNumber(num, 2n*(-s+n));
        x2 = new RatioNumber(num, 2n*(-s-n));

        temp = reviewFaster(x1, y1, q, ps.base, M, n, ps.p[0], ps.p[1], ps.p[2], red);
        if(temp)
            result.push(temp); 
          
        temp = reviewFaster(x2, y1, q, ps.base, M, n, ps.p[0], ps.p[1], ps.p[2], red);
        if(temp)
            result.push(temp);
    }

    //n=0   
    x1 = new RatioNumber(f, 2n*s);
    temp = reviewFaster(x1, y1, q, ps.base, M, 0, ps.p[0], ps.p[1], ps.p[2], red);
    if(temp)
        result.push(temp);

    return result;
}

function reviewFaster(x, y, q, base, m, n, A, B, C, r){ // Только для точек попадающих на сетку
    if((typeof(x.cMultiply(r).out()) == 'bigint' && typeof(y.cMultiply(r).out()) == 'bigint')){
        let temp = new Point(x, y);
        if(A.ratioDistance(temp, q, base) && B.ratioDistance(temp, q, base) && C.ratioDistance(temp, q, base))
            return {value: new Point(x.cMultiply(r).out(), y.cMultiply(r).out()), m:m*r, n:n*r};
    }
}

module.exports = {maximizeFasterWithReduce};



