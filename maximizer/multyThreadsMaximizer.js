const { Worker } = require('worker_threads')
const PointSet = require('../PointSet.js');
function runService(workerData) {
    let partition = getPartition(workerData.s, workerData.e, workerData.cores, workerData.step); 
    let workers = {};
    let promises = [];
    for(let i=0; i<partition.length-1; i++){
      workerData['s'] = partition[i];
      workerData['e'] = partition[i+1];
      promises.push(new Promise((resolve, reject) => {
        workers[i] = new Worker(__dirname+'/forWorkers.js', {workerData});
        workers[i].on('message', resolve);
        workers[i].on('error', reject);
        workers[i].on('exit', (code) => {
        if (code !== 0)
          reject(new Error(`Worker stopped with exit code ${code}`));
        })
    }));
    }
    return promises;
}

function getPartition(s, e, cores, step){
  let result = [0n];
  if((e-s)/step/cores === 0n)
    return getPartition(s, e, (e-s)/step, step);
  else{
    let curStep = BigInt(((e-s)/step/cores)*step);
    for(let i=1n; i<cores; i++)
      result.push(result[i-1n] + curStep);
    result.push(BigInt(e));
    return result;
  }
}

async function multyThreadsMaximizer(ps, cores=1n) {
  const promises = runService({s:0n, e:ps.distance(ps.p[0], ps.p[1])*ps.base, cores: cores, step:ps.base, PointSet:ps});
  let result = [];
  for(let prom of promises){
    let temp = await prom;
    result.push(...temp);
  }
  return result;
}

module.exports = {multyThreadsMaximizer};
