function hyperpola(A, B, M, char=1n, base=1n){
    if(typeof A.x == 'bigint' && typeof A.y == 'bigint')
        A = new Point(new RatioNumber(A.x, base), new RatioNumber(A.y, base));
    if(typeof B.x == 'bigint' && typeof B.y == 'bigint')
        B = new Point(new RatioNumber(B.x, base), new RatioNumber(B.y, base));

    let x1 = B.x, y1 = B.y, x2 = A.x, y2 = A.y ;

    let a1 = y1.multiply(y1).minus(y2.multiply(y2)).cMultiply(char).plus(x1.multiply(x1)).minus(x2.multiply(x2)).cMinus(M*M);
    let a2 = x2.minus(x1).multiply(x2.minus(x1)).cMinus(M*M).cMultiply(4n);
    let a3 = x2.minus(x1).multiply(a1).plus(x2.cMultiply(M*M)).cMultiply(4n);
    let a4 = y2.minus(y1).multiply(y2.minus(y1).cMultiply(char)).cMinus(M*M).cMultiply(4n);
    let a5 = y2.minus(y1).multiply(a1).plus(y2.cMultiply(M*M)).cMultiply(4n);
    let a6 = x2.minus(x1).multiply(y2.minus(y1)).cMultiply(8n);
    let a7 = y2.multiply(y2).cMultiply(char).plus(x2.multiply(x2)).cMultiply(4n*M*M).minus(a1.multiply(a1));
    

    return a2.out() + '*x^2 + ' + a3.out() + '*x + ' + a4.out() + '*y^2 + ' + a5.out() + '*sqrt('+ char +')*y + ' +
    a6.out() +'*sqrt('+ char +')*xy = ' + a7.out();
}

function xSort(arr){
    return arr.sort(function(a, b){
        let temp = a.value.x.p * b.value.x.q - b.value.x.p * a.value.x.q;
        if(temp>0)
            return 1;
        else if(temp == 0)
            return 0;
        else
            return -1;
    });
}

function ySort(arr){
    return arr.sort(function(a, b){
        let temp = a.value.y.p * b.value.y.q - b.value.y.p * a.value.y.q;
        if(temp>0)
            return 1;
        else if(temp == 0)
            return 0;
        else
            return -1;
    });
}

function arrayOut(a, base, char){
    for(let p of a)
        console.log('(' + p.value.x + '/' + base + ', ' + p.value.y + '*sqrt('+ char + ')' + '/' + base + ') m = ' + p.m + ' n = ' + p.n);   
}

module.exports = {
    arrayOut,
    xSort,
    ySort,
    hyperpola
}