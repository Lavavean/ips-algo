const isPerfectSquare = require('bigint-is-perfect-square');
var RatioNumber = require ( './RatioNumber.js' );
class Point{
    x;
    y;
    constructor(X, Y){
        this.x = X;
        this.y = Y;
    }

    ratioDistance(D, char, base){
        let temp1 = new RatioNumber ((this.x * D.x.q - D.x.p)*(this.x * D.x.q - D.x.p), D.x.q*D.x.q);
        let temp2 = new RatioNumber (char*(this.y * D.y.q - D.y.p)*(this.y * D.y.q - D.y.p), D.y.q*D.y.q);
        let temp = temp1.plus(temp2).cDivide(base*base).out();
        
        return (typeof(temp) == 'bigint' && isPerfectSquare(temp));
    }
    
    isEqualForIntPoints(A){ 
        return (this.x === A.x && this.y === A.y);
    }
}
module.exports = Point;