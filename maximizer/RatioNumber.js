const gcd = require('bigint-crypto-utils').gcd;
const abs = require('bigint-crypto-utils').abs;

class RatioNumber{
    p;
    q;
    constructor(P, Q){
        this.p = P;
        this.q = Q;
        this.reduce();
    }
    reduce(){
        if(this.p==0)
            this.q = 1n;
        else{
            if(this.q<0){
                this.p = (-1n)*this.p;
                this.q = (-1n)*this.q;
            }

            let temp = gcd(this.p, abs(this.q));
            let nod = temp == 0 ? 1n : temp;
            this.p /= nod;
            this.q /= nod;
        }
    }
    out(){
        if(this.q == 0)
            return Infinity;
        if(this.p % this.q == 0)
            return (this.p / this.q)
        else{
            this.reduce();
            return this.p + '/' + this.q;
        }
    }
    value(){
        return BigInt(this.p / this.q);
    }
    cPlus(c){
        let result = new RatioNumber(this.p + this.q*c, this.q);
        result.reduce();
        return result;
    }
    cMinus(c){
        let result = new RatioNumber(this.p - this.q*c, this.q);
        result.reduce();
        return result;
    }
    cMultiply(c){
        let result = new RatioNumber(this.p * c, this.q);
        result.reduce();
        return result;
    }
    cDivide(c){
        let result = new RatioNumber(this.p, this.q*c);
        result.reduce();
        return result;  
    }
    plus(r){
        let result = new RatioNumber(this.p * r.q + this.q * r.p, this.q * r.q);
        result.reduce();
        return result;
    }
    minus(r){
        let result = new RatioNumber(this.p * r.q - this.q * r.p, this.q * r.q);
        result.reduce();
        return result;
    }
    multiply(r){
        let result = new RatioNumber(this.p * r.p, this.q * r.q);
        result.reduce();
        return result;
    }
    divide(r){
        let result = new RatioNumber(this.p * r.q, this.q * r.p);
        result.reduce();
        return result;
    }
    isEqual(r){
        return (this.p * r.q === this.q * r.p);
    }
}

module.exports = RatioNumber;