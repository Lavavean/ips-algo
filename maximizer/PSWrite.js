const drawGnuplot = require('../drawGnuplot');
const ls = require('ls');
const fs = require('fs');
const crypto = require('crypto');
const JSON = require('bigint-json-native');
const mkdirp = require('mkdirp');

module.exports = function(ps, path){
    let json = JSON.stringify(ps);
    let hash = crypto.createHash('md5').update(json).digest("hex");
    let text = ps.prepareDataForGnuplot();
    mkdirp.sync(path);
    let filename = path + '/' +[
        ps.p.length,
        ps.diam(),
        ps.char,
        hash,
    ].join('_');

    fs.writeFileSync(filename+'.json', json);
    fs.writeFileSync(filename+'.txt', text);
    try{
        drawGnuplot(filename);
    } catch(e){
        console.log('Gnuplot error:');
        console.log(e);
    }
    fs.writeFileSync(filename+'.dist.txt', ps.distanceMatrix().map(r=>r.join('\t\t')).join('\n'));
	if(!ps.isIPS()){
		console.log('Error: not an IPS!  ' + filename);
	}
}