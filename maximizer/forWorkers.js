const { workerData, parentPort } = require('worker_threads')
const maximize = require('./maximize.js').maximize

let result = maximize(workerData.PointSet,workerData.s, workerData.e);
parentPort.postMessage(result);