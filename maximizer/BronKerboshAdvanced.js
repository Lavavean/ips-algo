const PointSet = require('../PointSet.js');

PointSet.prototype.newGeneration = function(arr, v, compsub){
    let PS = this;
    if(compsub.length < 2)
        return arr.filter(a => this.isDistanceIntegral(a, v));
    else{
        let result = arr.filter(function(a){
            let ps = new PointSet(PS.char, PS.base, new Array(...compsub));
            ps.p.push(a);
            return ps.isDistanceIntegral(a, v) && !ps.isContainsThreePointsOnLine() && !ps.isContainsFourPointsOnCircle();
        });
        return result
    }
};

function getCandidates(ps, newPoints){
    let candidates = [];
    for(let p of ps.p)
        candidates.push(p);
    for(let p of newPoints)
        candidates.push(p.value);
    return candidates;
};

function findMaxCliqueLenght(cliques){
    let max = cliques[0].p.length;
    for(let c of cliques){
        if(c.p.length > max)
            max = c.p.length;
    }
    return max;
};

function getResultFromCliques(cliques){
    let result = [];
    let max = findMaxCliqueLenght(cliques);
    for(let c of cliques){
        if(c.p.length === max)
            result.push(c);
    }
    return result;
};

function BronKerboshAdvanced(ps, newPoints){
    let cliques = [];
    let compsub = [];
    let candidates =  getCandidates(ps, newPoints);
    
    PointSet.prototype.findCliques = function(candidates){
        while(candidates.length){
            let v = candidates[0];
            compsub.push(v);
            let newCandidates = this.newGeneration(candidates, v, compsub);
            
            if(!newCandidates.length)
              cliques.push(new PointSet(this.char, this.base, new Array(...compsub)));
            else
                this.findCliques(newCandidates);
            
            compsub = compsub.filter(p => p !== v);
            candidates = candidates.filter(p => p !== v);
        }
    }
    ps.findCliques(candidates, []);

    return getResultFromCliques(cliques);
};

module.exports = {BronKerboshAdvanced}