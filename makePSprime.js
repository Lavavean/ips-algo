'use strict';
// Usage: node makePSprime.js filename.json

const fs = require('fs');
const JSON = require('bigint-json-native')({"BigNumber": BigInt, "noNew": true, "forceBig": true});
const PointSet = require('./PointSet');
const classifyAndWrite = require('./classifyAndWrite');

let filename = process.argv[2];

let ps = new PointSet();
ps.fromJSON(fs.readFileSync(filename,'utf8'));

//makePrime

ps.makePrime();

//write

classifyAndWrite(ps,{symm:true});
