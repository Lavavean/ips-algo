const fs = require('fs');

const files = process.argv.slice(2);

for (let file of files){
    console.log(file);
    let parsed = JSON.parse(fs.readFileSync(file,'utf8'));
    delete parsed.commonXpoints;
    delete parsed.dontAppendCopiesInAdvance;
    fs.writeFileSync(file,JSON.stringify(parsed));
}