'use strict';

// TODO: the primes table can contain BigInt, so needs fix
var primes = require('./primes.json').map(a => BigInt(a));


var primeSquares = primes.map(a => a**2n);
var maxPrime = primes[primes.length-1];

const abs = require('bigint-crypto-utils').abs;

function squarefree(n){
    if(n===0n){
        return [0n,0n];
    }
    n = abs(n);
    var multiplier = 1n;
    var residual = 1n;

    for(var h = 0; primeSquares[h] <= n && h < primes.length; h++){
        var i = primes[h];
        var j = primeSquares[h];
        while(n%j===0n){
            n/=j;
            multiplier*=i;
        }
        if(n%i===0n){
            n/=i;
            residual*=i;
        }
    }
    for(var i = maxPrime; i*i <= n ; i+=2n){
        var j = i*i;
        while(n%j===0n){
            n/=j;
            multiplier*=i;
        }
        if(n%i===0n){
            n/=i;
            residual*=i;
        }
    }
    return [multiplier, residual*n];
}

module.exports = squarefree;