const primes = require('./primes.json').map(a => BigInt(a));

class PrimeFactorization {
	constructor (number) {
		this.factors = [];
		if (!(number >= 2n)){
			return;
		}
		for (let prime of primes) {
			if (number%prime === 0n) {
				let pair = [prime,1n];
				number /= prime;
				while (number%prime === 0n) {
					pair[1]++;
					number /= prime;					
				}
				this.factors.push(pair);
			}
			if(number === 1n){
				break;
			}
		}
	}

	slice(){
		let copy = new PrimeFactorization(1n);
		for(let pair of this.factors) {
			copy.factors.push(pair.slice());
		}
		return copy;
	}

	enumerateDivisors(base, callback){
		if(!this.factors.length){
			callback(base);
			return;
		}
		let cropped = this.slice();
		let pair = cropped.factors.shift();
		for(let power = 0n; power <= pair[1]; power++){
			//console.log(cropped.factors,pair,power);
			let temp = cropped.slice();
			temp.enumerateDivisors(base*(pair[0]**power), callback);
		}
	}
};

module.exports = PrimeFactorization;


/*
console.log(new PrimeFactorization(6n));
console.log(new PrimeFactorization(12n));
console.log(new PrimeFactorization(96n));
console.log(new PrimeFactorization(17700n));
console.log(new PrimeFactorization(997n**20n*2n**87n));


new PrimeFactorization(12n).enumerateDivisors(1n,console.log);
new PrimeFactorization(96n).enumerateDivisors(1n,console.log);
//new PrimeFactorization(96n**20n).enumerateDivisors(1n,console.log);
*/