const PointSet = require('./PointSet');
const removeFromArray = require('./removeFromArray');


module.exports = class PointSetGraph extends PointSet{

    constructor() {
        super();
        this.zero = [];
        this.duaX = [];
        this.duaY = [];
        this.quad = [];
    }

    isTrivial(){
        return(
            this.quad.length === 0
        &&
            this.duaY.length === 0            
        )
    }

    countFriendsOnX(p){
        var friends = 0;
        for(var j = 0; j < this.duaX.length; j++){
            if(this.isDistanceIntegral(p, { x:  this.duaX[j].x,  y:  0n } ))
                friends++;
            if(this.isDistanceIntegral(p, { x: -this.duaX[j].x,  y:  0n } ))
                friends++;
        }
        if(this.zero.length){
            if(this.isDistanceIntegral(p, { x: 0n,  y:  0n } ))
                friends++;                
        }
        return friends;
    }

    countFriendsNotOnX(p){
        var friends = 0;
        for(var j = 0; j < this.quad.length; j++){
            // TODO: if the distance in vertical pair is not integral (e.g. char != 1),
            // the friends should be counted as one
            if(this.isDistanceIntegral(p, { x:  this.quad[j].x,  y:  this.quad[j].y } ))
                friends++;
            if(this.isDistanceIntegral(p, { x:  this.quad[j].x,  y: -this.quad[j].y } ))
                friends++;
            if(this.isDistanceIntegral(p, { x: -this.quad[j].x,  y:  this.quad[j].y } ))
                friends++;
            if(this.isDistanceIntegral(p, { x: -this.quad[j].x,  y: -this.quad[j].y } ))
                friends++;
        }
        for(var j = 0; j < this.duaY.length; j++){
            // TODO: if the distance in vertical pair is not integral (e.g. char != 1),
            // the friends should be counted as one
            if(this.isDistanceIntegral(p, { x:  0n,  y:  this.duaY[j].y } ))
                friends++;
            if(this.isDistanceIntegral(p, { x:  0n,  y: -this.duaY[j].y } ))
                friends++;
        }
        return friends;
    }

    countFriends(p){
        // There are already two friends (the base pair)
        var friends = 2 + this.countFriendsNotOnX(p);

        // TODO: separate function??
        if(p.y===0n){
            return friends + this.zero.length + this.duaX.length * 2;
        }

        return friends + this.countFriendsOnX(p);
    }

    getFriends(p){
        var friends = [];
        for(var j = 0; j < this.duaX.length; j++){
            if(this.isDistanceIntegral(p, { x:  this.duaX[j].x,  y:  0n } ))
                friends.push({ x:  this.duaX[j].x,  y:  0n });
            if(this.isDistanceIntegral(p, { x: -this.duaX[j].x,  y:  0n } ))
                friends.push({ x: -this.duaX[j].x,  y:  0n });
        }
        if(this.zero.length){
            if(this.isDistanceIntegral(p, this.zero[0] ))
                friends.push({x:0n,y:0n});
        }
        for(var j = 0; j < this.quad.length; j++){
            // TODO: if the distance in vertical pair is not integral (e.g. char != 1),
            // the friends should be counted as one
            if(this.isDistanceIntegral(p, { x:  this.quad[j].x,  y:  this.quad[j].y } ))
                friends.push({ x:  this.quad[j].x,  y:  this.quad[j].y });
            if(this.isDistanceIntegral(p, { x:  this.quad[j].x,  y: -this.quad[j].y } ))
                friends.push({ x:  this.quad[j].x,  y: -this.quad[j].y });
            if(this.isDistanceIntegral(p, { x: -this.quad[j].x,  y:  this.quad[j].y } ))
                friends.push({ x: -this.quad[j].x,  y:  this.quad[j].y });
            if(this.isDistanceIntegral(p, { x: -this.quad[j].x,  y: -this.quad[j].y } ))
                friends.push({ x: -this.quad[j].x,  y: -this.quad[j].y });
        }
        for(var j = 0; j < this.duaY.length; j++){
            // TODO: if the distance in vertical pair is not integral (e.g. char != 1),
            // the friends should be counted as one
            if(this.isDistanceIntegral(p, { x:  0n,  y:  this.duaY[j].y } ))
                friends.push({ x:  0n,  y:  this.duaY[j].y });
            if(this.isDistanceIntegral(p, { x:  0n,  y: -this.duaY[j].y } ))
                friends.push({ x:  0n,  y: -this.duaY[j].y });
        }
        return friends;        
    }

    getQuantityOfRecords(){
        return (
            this.zero.length +
            this.quad.length +
            this.duaX.length +
            this.duaY.length
        );
    }

    filterWhilePossible(friendsNeeded){
        while(true){
            var t = this.getQuantityOfRecords();

            this.filterQuad(friendsNeeded);
            this.filterZero(friendsNeeded);
            this.filterDuaX(friendsNeeded);
            this.filterDuaY(friendsNeeded);
            
            if(this.noFacher){
                this.filterFacherPoints();
            }

            if(this.noFacherDeeper){
                this.filterFacherPointsDeeper();
            }
            
            if (t == this.getQuantityOfRecords()){
                return;
            }
        }
    }

    filterQuad(friendsNeeded) {
        for(var i = 0; i < this.quad.length; i++){
            var friends = this.countFriends(this.quad[i]);
            if(friends < friendsNeeded){
                this.quad[i] = this.quad[this.quad.length - 1];
                this.quad.length--;
                i--;
            }
        }
    }

    filterDuaY(friendsNeeded) {
        for(var i = 0; i < this.duaY.length; i++){
            var friends = this.countFriends(this.duaY[i]);
            if(friends < friendsNeeded){
                this.duaY[i] = this.duaY[this.duaY.length - 1];
                this.duaY.length--;
                i--;
            }
        }
    }

    filterDuaX(friendsNeeded) {
        for(var i = 0; i < this.duaX.length; i++){
            var friends = this.countFriendsNotOnX(this.duaX[i]);
            if (
                friends === 0
            ||
                friends + 2 + this.zero.length + this.duaX.length * 2 < friendsNeeded
            ){
                this.duaX[i] = this.duaX[this.duaX.length - 1];
                this.duaX.length--;
                i--;
            }
        }
    }

    filterZero(friendsNeeded) {
        if(!this.zero.length){
            return;
        }
        var friends = this.countFriendsNotOnX(this.zero[0]);
        if (
            friends === 0
        ||
            friends + 2 + 1 + this.duaX.length * 2 < friendsNeeded
        ){
            this.zero.length = 0;
        }
    }

    filterFacherPoints(){
        for(let arr of [this.quad, this.duaY]){
            for(var i = 0; i < arr.length; i++){
                var friends = this.countFriendsNotOnX(arr[i]);
                if(friends == 1){
                    arr[i] = arr[arr.length - 1];
                    arr.length--;
                    i--;
                }
            }            
        }
    }

    filterFacherPointsDeeper(){
        // this.base is divisible by 2
        var base = this.base/2n;
        var ps = new PointSet();
        ps.char = this.char;
        for(let arr of [this.quad, this.duaY]){
            for(var i = 0; i < arr.length; i++){
                var friends = this.getFriends(arr[i]);
                //TODO: this is very slow to run, but fast to write code :/
                ps.p = friends;
                ps.base = this.base;
                ps = ps.deepClone();
                ps.shift(base**2n,0n);
                ps.rotate(0);
                let j;
                for(j = 0; j < ps.p.length; j++){
                    if(ps.p[j].y)
                        break;
                }
                if(j == ps.p.length){
                    removeFromArray(arr,i);
                    i--;
                    continue;
                }

                ps.p = friends;
                ps.base = this.base;
                ps = ps.deepClone();
                ps.shift(-(base**2n),0n);
                ps.rotate(0);
                for(j = 0; j < ps.p.length; j++){
                    if(ps.p[j].y)
                        break;
                }
                if(j == ps.p.length){
                    removeFromArray(arr,i);
                    i--;
                    continue;
                }
            }            
        }        
    }

    filterCrossPoints(){
        // this.base is divisible by 2
        var base = this.base/2n;
        var ps = new PointSet();
        ps.char = this.char;
        for(let arr of [this.quad, this.duaY]){
            for(var i = 0; i < arr.length; i++){
                var friends = this.getFriends(arr[i]);
                let j;
                for(j = 0; j < friends.length; j++){
                    if(friends[j].y && friends[j].x != arr[i].x)
                        break;
                }
                if(j == friends.length){
                    removeFromArray(arr,i);
                    i--;
                    continue;
                }

                //TODO: this is very slow to run, but fast to write code :/
                ps.p = friends.slice();
                ps.p.push(arr[i]);
                ps.base = this.base;
                ps = ps.deepClone();
                ps.shift(base**2n,0n);
                ps.rotate(0);
                var x = ps.p[ps.p.length - 1].x;
                for(j = 0; j < ps.p.length; j++){
                    if(ps.p[j].y && ps.p[j].x != x)
                        break;
                }
                if(j == ps.p.length){
                    removeFromArray(arr,i);
                    i--;
                    continue;
                }

                ps.p = friends.slice();
                ps.p.push(arr[i]);
                ps.base = this.base;
                ps = ps.deepClone();
                ps.shift(-(base**2),0);
                ps.rotate(0);
                var x = ps.p[ps.p.length - 1].x;
                for(j = 0; j < ps.p.length; j++){
                    if(ps.p[j].y && ps.p[j].x != x)
                        break;
                }
                if(j == ps.p.length){
                    removeFromArray(arr,i);
                    i--;
                    continue;
                }
            }            
        }        
    }
}

