
var child_process = require('child_process');
module.exports = function drawGnuplot(filename){
	for(let ext of ['png','eps']){
		child_process.execSync(
			'gnuplot',
			{
				input: [
					'set nokey',
					'set size ratio -1',
					'set tics font ",25"',
					'set lmargin 12',
					'set rmargin  6',
					'set term '+ext+' size 2600,1536 enhanced #crop',
					'set output "' + filename + '.'+ext+'"',
					'plot "' + filename + '.txt" ps 5 pt 13',
					'exit',
				''].join('\n'),
			}
		)
	}
}
